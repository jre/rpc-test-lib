<?php
declare(strict_types = 1);

namespace Lib\Test;


/**
 * 更新余额
 * @author mg
 *
 */
interface IUpdateMoney
{
	public function parse( float $money): bool;
}